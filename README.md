# Aplikasi Iuran Kas RT

Yang dibutuhkan
 -  PHP V7 - V8
 -  Laravel 8

 ## Instalasi

 1. Ke folder htdocs, buka CMD kemudian Pull repository 
 
```
 `https://gitlab.com/wisnupriester/iuran-kas`
```
 
 2. CD ke directory projek 
 ```
cd iuran_kas
 ```

 3. Instal dependency composer, bisa di CMD atau Terminal di VSCode setelah open folder.
 ```
composer install
 ```

 4. Di VSCode atau compiler, buka file .env-exempel kemudian atur database confignya seperti dibawah ini.
 ```
database.default.hostname = localhost
database.default.database = iuran_kas
database.default.username = root
database.default.password =
database.default.DBDriver = MySQLi
```

5. Buka PHPmyAdmin, bikin database iuran_kas seperti yang di atas.

6. Lakukan Migrasi Database lewat konsol terminal dengan command ini.
```
php spark migrate
```

7. Jalankan aplikasi dengan perintah ini.

```
php spark serve
```

Sekarang buka browser dengan alamat address http://localhost:8080/
